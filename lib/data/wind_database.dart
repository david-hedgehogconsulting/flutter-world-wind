import 'package:flutter_wind/services/unzipper.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'wind_database.g.dart';

@DataClassName("City")
class Cities extends Table {
  IntColumn get id => integer()();
  TextColumn get name => text()();
  TextColumn get country => text().withLength(min: 2, max: 2)();

  @override
  Set<Column> get primaryKey => {id};
}

@DataClassName("LookupCity")
class LookupCities extends Table {
  IntColumn get id => integer()();
  TextColumn get name => text()();
  TextColumn get country => text().withLength(min: 2, max: 2)();

  @override
  Set<Column> get primaryKey => {id};
}

@UseMoor(tables: [Cities, LookupCities])
class WindDatabase extends _$WindDatabase {
  WindDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(onCreate: (Migrator m) {
      return m.createAllTables();
    }, beforeOpen: (details) async {
      if (details.wasCreated) {
        final Unzipper unzipper = Unzipper();
        final cityStream = unzipper.unzip("assets/files/city.list.json.gz.bin");
        List<City> cityList = await cityStream.toList();
        List<LookupCitiesCompanion> lookupCityList = cityList
            .where((city) => city.country.length == 2)
            .map((city) => LookupCitiesCompanion(
                id: Value(city.id),
                name: Value(city.name),
                country: Value(city.country)))
            .toList();
        await into(lookupCities).insertAll(lookupCityList);
        print("${lookupCityList.length} lookup records inserted");
      }
    }, onUpgrade: (m, from, to) async {
      print("from: $from to: $to");
    });
  }

  Future insertLookupCities(Stream<City> cities) async {
    return transaction(() async {
      cities.listen((city) async {
        await into(lookupCities).insert(LookupCitiesCompanion(
            id: Value(city.id),
            name: Value(city.name),
            country: Value(city.country)));
      });
    });
  }
}

@UseDao(tables: [Cities])
class CitiesDao extends DatabaseAccessor<WindDatabase> with _$CitiesDaoMixin {
  final WindDatabase db;

  CitiesDao(this.db) : super(db);

  Stream<List<City>> observeCities() => select(cities).watch();
  Future<List<City>> fetchCities() => select(cities).get();
  Future insertCity(Insertable<City> city) => into(cities).insert(city);
  Future deleteCity(Insertable<City> city) => delete(cities).delete(city);
}

@UseDao(tables: [LookupCities])
class LookupCityDao extends DatabaseAccessor<WindDatabase>
    with _$LookupCityDaoMixin {
  final WindDatabase db;

  LookupCityDao(this.db) : super(db);

  Future<List<LookupCity>> fetchCities() => select(lookupCities).get();
  Future insertCity(Insertable<LookupCity> city) =>
      into(lookupCities).insert(city);
  Future deleteCity(Insertable<LookupCity> city) =>
      delete(lookupCities).delete(city);

  Future<
      List<LookupCity>> searchForCities(String searchString) => customSelectQuery(
          "SELECT * FROM lookup_cities WHERE name LIKE '$searchString%' ORDER BY name")
      .map((row) => LookupCity.fromData(row.data, db))
      .get();
}

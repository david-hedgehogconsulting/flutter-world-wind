// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wind_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class City extends DataClass implements Insertable<City> {
  final int id;
  final String name;
  final String country;
  City({@required this.id, @required this.name, @required this.country});
  factory City.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return City(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      country:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}country']),
    );
  }
  factory City.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return City(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      country: serializer.fromJson<String>(json['country']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'country': serializer.toJson<String>(country),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<City>>(bool nullToAbsent) {
    return CitiesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      country: country == null && nullToAbsent
          ? const Value.absent()
          : Value(country),
    ) as T;
  }

  City copyWith({int id, String name, String country}) => City(
        id: id ?? this.id,
        name: name ?? this.name,
        country: country ?? this.country,
      );
  @override
  String toString() {
    return (StringBuffer('City(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('country: $country')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(name.hashCode, country.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is City &&
          other.id == id &&
          other.name == name &&
          other.country == country);
}

class CitiesCompanion extends UpdateCompanion<City> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> country;
  const CitiesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.country = const Value.absent(),
  });
  CitiesCompanion copyWith(
      {Value<int> id, Value<String> name, Value<String> country}) {
    return CitiesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      country: country ?? this.country,
    );
  }
}

class $CitiesTable extends Cities with TableInfo<$CitiesTable, City> {
  final GeneratedDatabase _db;
  final String _alias;
  $CitiesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _countryMeta = const VerificationMeta('country');
  GeneratedTextColumn _country;
  @override
  GeneratedTextColumn get country => _country ??= _constructCountry();
  GeneratedTextColumn _constructCountry() {
    return GeneratedTextColumn('country', $tableName, false,
        minTextLength: 2, maxTextLength: 2);
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, country];
  @override
  $CitiesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'cities';
  @override
  final String actualTableName = 'cities';
  @override
  VerificationContext validateIntegrity(CitiesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (name.isRequired && isInserting) {
      context.missing(_nameMeta);
    }
    if (d.country.present) {
      context.handle(_countryMeta,
          country.isAcceptableValue(d.country.value, _countryMeta));
    } else if (country.isRequired && isInserting) {
      context.missing(_countryMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  City map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return City.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(CitiesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.country.present) {
      map['country'] = Variable<String, StringType>(d.country.value);
    }
    return map;
  }

  @override
  $CitiesTable createAlias(String alias) {
    return $CitiesTable(_db, alias);
  }
}

class LookupCity extends DataClass implements Insertable<LookupCity> {
  final int id;
  final String name;
  final String country;
  LookupCity({@required this.id, @required this.name, @required this.country});
  factory LookupCity.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return LookupCity(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      country:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}country']),
    );
  }
  factory LookupCity.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return LookupCity(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      country: serializer.fromJson<String>(json['country']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'country': serializer.toJson<String>(country),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<LookupCity>>(bool nullToAbsent) {
    return LookupCitiesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      country: country == null && nullToAbsent
          ? const Value.absent()
          : Value(country),
    ) as T;
  }

  LookupCity copyWith({int id, String name, String country}) => LookupCity(
        id: id ?? this.id,
        name: name ?? this.name,
        country: country ?? this.country,
      );
  @override
  String toString() {
    return (StringBuffer('LookupCity(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('country: $country')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(name.hashCode, country.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is LookupCity &&
          other.id == id &&
          other.name == name &&
          other.country == country);
}

class LookupCitiesCompanion extends UpdateCompanion<LookupCity> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> country;
  const LookupCitiesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.country = const Value.absent(),
  });
  LookupCitiesCompanion copyWith(
      {Value<int> id, Value<String> name, Value<String> country}) {
    return LookupCitiesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      country: country ?? this.country,
    );
  }
}

class $LookupCitiesTable extends LookupCities
    with TableInfo<$LookupCitiesTable, LookupCity> {
  final GeneratedDatabase _db;
  final String _alias;
  $LookupCitiesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _countryMeta = const VerificationMeta('country');
  GeneratedTextColumn _country;
  @override
  GeneratedTextColumn get country => _country ??= _constructCountry();
  GeneratedTextColumn _constructCountry() {
    return GeneratedTextColumn('country', $tableName, false,
        minTextLength: 2, maxTextLength: 2);
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, country];
  @override
  $LookupCitiesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'lookup_cities';
  @override
  final String actualTableName = 'lookup_cities';
  @override
  VerificationContext validateIntegrity(LookupCitiesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (name.isRequired && isInserting) {
      context.missing(_nameMeta);
    }
    if (d.country.present) {
      context.handle(_countryMeta,
          country.isAcceptableValue(d.country.value, _countryMeta));
    } else if (country.isRequired && isInserting) {
      context.missing(_countryMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  LookupCity map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return LookupCity.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(LookupCitiesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.country.present) {
      map['country'] = Variable<String, StringType>(d.country.value);
    }
    return map;
  }

  @override
  $LookupCitiesTable createAlias(String alias) {
    return $LookupCitiesTable(_db, alias);
  }
}

abstract class _$WindDatabase extends GeneratedDatabase {
  _$WindDatabase(QueryExecutor e)
      : super(const SqlTypeSystem.withDefaults(), e);
  $CitiesTable _cities;
  $CitiesTable get cities => _cities ??= $CitiesTable(this);
  $LookupCitiesTable _lookupCities;
  $LookupCitiesTable get lookupCities =>
      _lookupCities ??= $LookupCitiesTable(this);
  @override
  List<TableInfo> get allTables => [cities, lookupCities];
}

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CitiesDaoMixin on DatabaseAccessor<WindDatabase> {
  $CitiesTable get cities => db.cities;
}

mixin _$LookupCityDaoMixin on DatabaseAccessor<WindDatabase> {
  $LookupCitiesTable get lookupCities => db.lookupCities;
}

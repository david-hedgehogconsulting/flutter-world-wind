import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wind/bloc/search/search_event.dart';
import 'package:flutter_wind/bloc/search/search_state.dart';
import 'package:flutter_wind/data/wind_database.dart';

class CitySearch extends SearchDelegate<LookupCity> {
  final Bloc<SearchEvent, SearchState> searchBloc;

  CitySearch(this.searchBloc);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    searchBloc.dispatch(Search(query));

    return BlocBuilder(
      bloc: searchBloc,
      builder: (BuildContext context, SearchState state) {
        if (state is SearchLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is SearchError) {
          return Container(
            child: Text('Error'),
          );
        }
        if (state is SearchResults) {
          return ListView.builder(
            itemBuilder: (context, index) {
              return ListTile(
                leading: Icon(
                  Icons.location_city,
                  color: Colors.blue,
                ),
                title: Text(state.results[index].name),
                subtitle: Text(state.results[index].country),
                onTap: () => close(context, state.results[index]),
              );
            },
            itemCount: state.results.length,
          );
        }
        return Container();
      },
    );
  }
}

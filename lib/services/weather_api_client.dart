import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_wind/model/wind.dart';
import 'package:flutter_wind/util/secrets.dart';
import 'package:http/http.dart' as http;

class WeatherApiClient {
  static const baseUrl = 'http://api.openweathermap.org/data/2.5/';
  final http.Client httpClient;
  final Secrets secrets = Secrets();

  WeatherApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Wind>> fetchWind(List<int> cityIds) async {
    final apiKey = await secrets.loadSecret("weather_api_key");
    final citiesStr = cityIds.join(',');
    final response = await httpClient
        .get('${baseUrl}group?id=$citiesStr&units=metric&appid=$apiKey');
    if (response.statusCode == 200) {
      try {
        final data = json.decode(response.body) as Map<String, dynamic>;
        final list = data['list'] as List;
        final windList = list.map((rawWind) {
          return Wind(
            id: rawWind['id'],
            city: rawWind['name'],
            country: rawWind['sys']['country'],
            speed: rawWind['wind']['speed'],
            degrees: rawWind['wind']['deg'] ?? 0,
          );
        }).toList();
        return windList;
      } catch (e) {
        throw e;
      }
    } else {
      throw Exception('error fetching wind');
    }
  }

  Future<List<Wind>> fetchForecastForCity(int cityId) async {
    final apiKey = await secrets.loadSecret("weather_api_key");
    final response = await httpClient
        .get('${baseUrl}forecast?id=$cityId&units=metric&appid=$apiKey');
    if (response.statusCode == 200) {
      try {
        final data = json.decode(response.body) as Map<String, dynamic>;
        final list = data['list'] as List;
        final windList = list.map((rawWind) {
          return Wind(
            id: rawWind['id'],
            city: rawWind['name'],
            country: rawWind['sys']['country'],
            speed: rawWind['wind']['speed'],
            degrees: rawWind['wind']['deg'] ?? 0,
            dateTime: DateTime.fromMillisecondsSinceEpoch(rawWind['dt'] * 1000),
          );
        }).toList();
        return windList;
      } catch (e) {
        throw e;
      }
    } else {
      throw Exception('error fetching forecast');
    }
  }
}

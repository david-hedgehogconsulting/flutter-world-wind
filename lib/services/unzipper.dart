import 'dart:async';
import 'dart:convert';

import 'package:archive/archive.dart';
import 'package:flutter/services.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:http/http.dart';
import 'package:moor_flutter/moor_flutter.dart';

class Unzipper {
  Stream<City> unzip(String assetName) async* {
    final ByteData data = await rootBundle.load(assetName);
    final Uint8List list =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    final unzipped = GZipDecoder().decodeBytes(list);
    final ByteStream bs = ByteStream.fromBytes(unzipped);
    final String dataString = await bs.bytesToString();
    final List<dynamic> cityList = json.decode(dataString);
    for (var item in cityList) {
      yield City.fromJson(item);
    }
  }
}

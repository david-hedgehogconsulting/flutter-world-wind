import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable {
  SearchEvent([List props = const []]) : super(props);
}

class Search extends SearchEvent {
  final String searchString;

  Search(this.searchString) : super([searchString]);

  @override
  String toString() => 'Search';
}

import 'package:equatable/equatable.dart';
import 'package:flutter_wind/data/wind_database.dart';

abstract class SearchState extends Equatable {
  SearchState([List props = const []]) : super(props);
}

class SearchUninitialized extends SearchState {
  @override
  String toString() => 'SearchUninitialized';
}

class SearchError extends SearchState {
  @override
  String toString() => 'SearchError';
}

class SearchLoading extends SearchState {
  @override
  String toString() => 'SearchLoading';
}

class SearchResults extends SearchState {
  final List<LookupCity> results;

  SearchResults(this.results) : super([results]);

  @override
  String toString() => 'SearchResults';
}

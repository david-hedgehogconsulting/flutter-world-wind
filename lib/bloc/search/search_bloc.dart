import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_wind/bloc/search/search_event.dart';
import 'package:flutter_wind/bloc/search/search_state.dart';
import 'package:flutter_wind/repositories/search_repository.dart';
import 'package:meta/meta.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchRepository searchRepository;

  SearchBloc({@required this.searchRepository});

  @override
  Stream<SearchState> transform(
    Stream<SearchEvent> events,
    Stream<SearchState> Function(SearchEvent event) next,
  ) {
    return super.transform(
      events,
      next,
    );
  }

  @override
  get initialState => SearchUninitialized();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    try {
      print("${DateTime.now().toIso8601String()} event: $event");
      if (event is Search) {
        if (event.searchString.isEmpty) {
          yield SearchResults([]);
        } else {
          yield SearchLoading();
          final cityList =
              await searchRepository.searchForCities(event.searchString);
          yield SearchResults(cityList);
        }
      }
      return;
    } catch (e) {
      yield SearchError();
    }
  }
}

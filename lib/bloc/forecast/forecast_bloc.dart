import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_wind/bloc/forecast/forecast_state.dart';
import 'package:flutter_wind/repositories/forecast_repository.dart';
import 'package:meta/meta.dart';

import 'forecast_event.dart';

class ForecastBloc extends Bloc<ForecastEvent, ForecastState> {
  final ForecastRepository forecastRepository;

  ForecastBloc({@required this.forecastRepository});

  @override
  Stream<ForecastState> transform(
    Stream<ForecastEvent> events,
    Stream<ForecastState> Function(ForecastEvent event) next,
  ) {
    return super.transform(
      events,
      next,
    );
  }

  @override
  get initialState => ForecastUninitialized();

  @override
  Stream<ForecastState> mapEventToState(ForecastEvent event) async* {
    try {
      print("${DateTime.now().toIso8601String()} event: $event");
      if (event is Fetch) {
        final windList =
            await forecastRepository.fetchForecastForCity(event.city.id);
        yield ForecastLoaded(windList);
      }
      return;
    } catch (e) {
      yield ForecastError();
    }
  }
}

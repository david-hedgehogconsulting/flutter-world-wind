import 'package:equatable/equatable.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/model/wind.dart';

abstract class ForecastEvent extends Equatable {
  ForecastEvent([List props = const []]) : super(props);
}

class Fetch extends ForecastEvent {
  final City city;

  Fetch(this.city);

  @override
  String toString() => 'Fetch';
}

class DataChanged extends ForecastEvent {
  final List<Wind> windList;

  DataChanged(this.windList) : super([windList]);

  @override
  String toString() => 'DataChanged';
}

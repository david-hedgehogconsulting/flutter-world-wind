import 'package:equatable/equatable.dart';
import 'package:flutter_wind/model/wind.dart';

abstract class ForecastState extends Equatable {
  ForecastState([List props = const []]) : super(props);
}

class ForecastUninitialized extends ForecastState {
  @override
  String toString() => 'ForecastUninitialized';
}

class ForecastError extends ForecastState {
  @override
  String toString() => 'ForecastError';
}

class ForecastLoaded extends ForecastState {
  final List<Wind> windList;

  ForecastLoaded(this.windList);

  @override
  String toString() => 'ForecastLoaded';
}

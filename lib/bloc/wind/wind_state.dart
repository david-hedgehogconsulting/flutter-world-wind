import 'package:equatable/equatable.dart';
import 'package:flutter_wind/model/city_wind.dart';

abstract class WindState extends Equatable {
  WindState([List props = const []]) : super(props);
}

class WindUninitialized extends WindState {
  @override
  String toString() => 'WindUninitialized';
}

class WindError extends WindState {
  @override
  String toString() => 'WindError';
}

class WindLoaded extends WindState {
  final List<CityWind> windList;
  final bool hasReachedMax;
  final int timestamp;

  WindLoaded({this.windList, this.hasReachedMax, this.timestamp})
      : super([windList, hasReachedMax, timestamp]);

  WindLoaded copyWith({
    List<CityWind> windList,
    bool hasReachedMax,
  }) {
    return WindLoaded(
      windList: windList ?? this.windList,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() =>
      'WindLoaded { count: ${windList.length}, hasReachedMax: $hasReachedMax }';
}

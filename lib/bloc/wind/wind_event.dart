import 'package:equatable/equatable.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/model/city_wind.dart';

abstract class WindEvent extends Equatable {
  WindEvent([List props = const []]) : super(props);
}

class Fetch extends WindEvent {
  @override
  String toString() => 'Fetch';
}

class DataChanged extends WindEvent {
  final List<CityWind> windList;

  DataChanged(this.windList) : super([windList]);

  @override
  String toString() => 'DataChanged';
}

class DeleteCity extends WindEvent {
  final City city;

  DeleteCity(this.city) : super([city]);

  @override
  String toString() => 'DeleteCity';
}

class InsertLookupCity extends WindEvent {
  final LookupCity city;

  InsertLookupCity(this.city) : super([city]);

  @override
  String toString() => 'InsertLookupCity';
}

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_wind/bloc/wind/wind_event.dart';
import 'package:flutter_wind/bloc/wind/wind_state.dart';
import 'package:flutter_wind/repositories/wind_repository.dart';
import 'package:meta/meta.dart';

class WindBloc extends Bloc<WindEvent, WindState> {
  final WindRepository windRepository;

  StreamSubscription subscription;

  WindBloc({@required this.windRepository});

  @override
  Stream<WindState> transform(
    Stream<WindEvent> events,
    Stream<WindState> Function(WindEvent event) next,
  ) {
    return super.transform(
      events,
      next,
    );
  }

  @override
  get initialState => WindUninitialized();

  @override
  Stream<WindState> mapEventToState(WindEvent event) async* {
    try {
      print("${DateTime.now().toIso8601String()} event: $event");
      if (event is Fetch) {
        if (currentState is WindUninitialized) {
          subscription?.cancel();
          subscription = windRepository.observeCityWind().listen((windList) {
            print("${DateTime.now().toIso8601String()} windList: $windList");
            dispatch(DataChanged(windList));
          });
        } else if (currentState is WindLoaded) {
          final windList = await windRepository.fetchCityWind();
          yield windList.isEmpty
              ? (currentState as WindLoaded).copyWith(hasReachedMax: true)
              : WindLoaded(
                  windList: windList,
                  hasReachedMax: true,
                  timestamp: DateTime.now().millisecondsSinceEpoch,
                );
        }
        return;
      }
      if (event is DataChanged) {
        yield WindLoaded(
          windList: event.windList,
          hasReachedMax: true,
          timestamp: DateTime.now().millisecondsSinceEpoch,
        );
        return;
      }
      if (event is DeleteCity) {
        final int result = await windRepository.deleteCity(event.city);
        print(
            "${DateTime.now().toIso8601String()} DeleteCity city: ${event.city} result: $result");
      }
      if (event is InsertLookupCity) {
        final int result = await windRepository.insertLookupCity(event.city);
        print(
            "${DateTime.now().toIso8601String()} InsertCity city: ${event.city} result: $result");
      }
    } catch (e) {
      yield WindError();
    }
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }
}

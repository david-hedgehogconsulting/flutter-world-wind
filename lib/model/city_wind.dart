import 'package:equatable/equatable.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/model/wind.dart';

class CityWind extends Equatable {
  final City city;
  final Wind wind;

  CityWind(this.city, this.wind) : super([city.id, wind?.speed, wind?.degrees]);

  @override
  String toString() => 'CityWind { city.id: ${city.id}, wind: $wind }';
}

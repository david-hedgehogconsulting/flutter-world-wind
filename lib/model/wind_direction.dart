enum WindDirection {
  south,
  southwest,
  west,
  northwest,
  north,
  northeast,
  east,
  southeast
}

class WindDirectionHelper {
  static const num _DEGREES_360 = 360;

  static WindDirection fromDegrees({num degrees}) {
    assert(degrees >= 0);
    final num deg = degrees + _DEGREES_360 / (WindDirection.values.length * 2);
    final int degInt = deg.round() % _DEGREES_360;
    final int index =
        (degInt * WindDirection.values.length / _DEGREES_360).floor();
    return WindDirection.values[index];
  }

  static String toIconAsset(WindDirection windDirection) {
    switch (windDirection) {
      case WindDirection.south:
        return 'assets/images/wind_disc3.png';
      case WindDirection.southwest:
        return 'assets/images/wind_disc7.png';
      case WindDirection.west:
        return 'assets/images/wind_disc4.png';
      case WindDirection.northwest:
        return 'assets/images/wind_disc8.png';
      case WindDirection.north:
        return 'assets/images/wind_disc1.png';
      case WindDirection.northeast:
        return 'assets/images/wind_disc5.png';
      case WindDirection.east:
        return 'assets/images/wind_disc2.png';
      case WindDirection.southeast:
        return 'assets/images/wind_disc6.png';
      default:
        throw Exception("Invalid Wind Direction");
    }
  }
}

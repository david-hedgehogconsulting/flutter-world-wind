import 'package:equatable/equatable.dart';

class Wind extends Equatable {
  final int id;
  final String city;
  final String country;
  final num speed;
  final num degrees;
  final DateTime dateTime;

  Wind(
      {this.id,
      this.city,
      this.country,
      this.speed,
      this.degrees,
      this.dateTime})
      : super([id, city, country, speed, degrees, dateTime]);

  @override
  String toString() => '{ id: $id speed: $speed degrees: $degrees';
}

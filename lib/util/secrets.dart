import 'dart:convert';

import 'package:flutter/services.dart';

class Secrets {
  static const String SECRETS_PATH = "assets/files/secrets.json";

  final Map<String, String> _secrets = Map<String, String>();

  Future<String> loadSecret(String key) async {
    return _secrets[key] ??
        rootBundle.loadStructuredData(SECRETS_PATH, (jsonStr) async {
          final Map<String, dynamic> jsonMap = json.decode(jsonStr);
          final secret = jsonMap[key];
          _secrets[key] = secret;
          return secret;
        });
  }
}

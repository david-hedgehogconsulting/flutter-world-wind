import 'package:flutter/material.dart';

class ScrollableText extends StatelessWidget {
  final String data;

  ScrollableText(this.data);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Container(
        child: Center(
          child: Text(data),
        ),
        height: MediaQuery.of(context).size.height,
      ),
    );
  }
}

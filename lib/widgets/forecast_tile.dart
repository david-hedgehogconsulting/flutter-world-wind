import 'package:flutter/material.dart';
import 'package:flutter_wind/model/wind_direction.dart';
import 'package:flutter_wind/widgets/wind_icon.dart';

import 'package:flutter_wind/model/wind.dart';
import 'package:intl/intl.dart';

class ForecastTile extends StatelessWidget {
  final Wind wind;

  const ForecastTile({Key key, @required this.wind}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: WindIcon(
        windSpeed: wind?.speed?.round() ?? 0,
        windDirection: WindDirectionHelper.fromDegrees(
          degrees: wind?.degrees ?? 0,
        ),
      ),
      title: Text(DateFormat.Hm().format(wind.dateTime)),
      isThreeLine: false,
      subtitle: Text(DateFormat('EEEE d MMMM').format(wind.dateTime)),
      dense: true,
      contentPadding: EdgeInsets.only(left: 0),
    );
  }
}

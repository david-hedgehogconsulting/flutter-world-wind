import 'package:flutter/material.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/model/wind_direction.dart';
import 'package:flutter_wind/widgets/wind_icon.dart';

import 'package:flutter_wind/model/wind.dart';

class WindTile extends StatelessWidget {
  final City city;
  final Wind wind;
  final GestureTapCallback onTap;

  const WindTile(
      {Key key, @required this.city, @required this.wind, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: WindIcon(
        windSpeed: wind?.speed?.round() ?? 0,
        windDirection: WindDirectionHelper.fromDegrees(
          degrees: wind?.degrees ?? 0,
        ),
      ),
      title: Text('${city.name}'),
      isThreeLine: false,
      subtitle: Text(city.country),
      dense: true,
      contentPadding: EdgeInsets.only(left: 0),
      onTap: onTap,
    );
  }
}

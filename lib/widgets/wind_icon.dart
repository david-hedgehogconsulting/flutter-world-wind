import 'package:flutter/material.dart';
import 'package:flutter_wind/model/wind_direction.dart';

class WindIcon extends StatelessWidget {
  final int windSpeed;
  final WindDirection windDirection;

  WindIcon({@required this.windSpeed, @required this.windDirection});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(
        height: 56,
        width: 56,
      ),
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            WindDirectionHelper.toIconAsset(windDirection),
            color: Colors.blue,
            fit: BoxFit.contain,
          ),
          Center(
            child: Text(
              "$windSpeed",
              style: TextStyle(
                color: Colors.white,
                fontSize: 11,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}

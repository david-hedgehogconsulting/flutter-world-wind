import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wind/bloc/search/search_bloc.dart';
import 'package:flutter_wind/bloc/wind/wind_event.dart';
import 'package:flutter_wind/repositories/search_repository.dart';
import 'package:flutter_wind/repositories/wind_repository.dart';
import 'package:flutter_wind/search/city_search.dart';
import 'package:flutter_wind/services/weather_api_client.dart';
import 'package:flutter_wind/views/cities_page.dart';
import 'package:http/http.dart' as http;

import 'bloc/wind/wind_bloc.dart';
import 'data/wind_database.dart';

void main() {
  final WindDatabase windDatabase = WindDatabase();
  final WindRepository windRepository = WindRepository(
    citiesDao: CitiesDao(windDatabase),
    weatherApiClient: WeatherApiClient(httpClient: http.Client()),
  );
  final SearchRepository searchRepository = SearchRepository(
    lookupCitiesDao: LookupCityDao(windDatabase),
  );

  runApp(WindApp(
    windRepository: windRepository,
    searchRepository: searchRepository,
  ));
}

class WindApp extends StatelessWidget {
  final WindRepository windRepository;
  final SearchRepository searchRepository;

  WindApp(
      {Key key, @required this.windRepository, @required this.searchRepository})
      : assert(windRepository != null && searchRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: HomeScreen(
      windRepository: windRepository,
      searchRepository: searchRepository,
    ));
  }
}

class HomeScreen extends StatelessWidget {
  final WindRepository windRepository;
  final SearchRepository searchRepository;

  HomeScreen({
    Key key,
    @required this.windRepository,
    @required this.searchRepository,
  })  : assert(windRepository != null && searchRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final WindBloc windBloc = WindBloc(windRepository: windRepository);

    return MaterialApp(
      title: 'Flutter World Wind',
      home: BlocProvider(
        builder: (context) => windBloc,
        child: Scaffold(
          appBar: AppBar(
            title: Text('World Wind'),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            elevation: 2.0,
            onPressed: () async {
              final LookupCity searchCity = await showSearch(
                context: context,
                delegate:
                    CitySearch(SearchBloc(searchRepository: searchRepository)),
              );
              if (searchCity != null) {
                windBloc.dispatch(InsertLookupCity(searchCity));
              }
            },
            tooltip: 'Add something',
          ),
          body: CitiesPage(),
        ),
      ),
    );
  }
}

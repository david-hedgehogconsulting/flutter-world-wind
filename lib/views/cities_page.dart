import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_wind/bloc/forecast/forecast_bloc.dart';
import 'package:flutter_wind/bloc/wind/wind_bloc.dart';
import 'package:flutter_wind/bloc/wind/wind_event.dart';
import 'package:flutter_wind/bloc/wind/wind_state.dart';
import 'package:flutter_wind/repositories/forecast_repository.dart';
import 'package:flutter_wind/services/weather_api_client.dart';
import 'package:flutter_wind/views/forecast_page.dart';
import 'package:flutter_wind/widgets/bottom_loader.dart';
import 'package:flutter_wind/widgets/scrollable_text.dart';
import 'package:flutter_wind/widgets/wind_tile.dart';
import 'package:http/http.dart' as http;

class CitiesPage extends StatefulWidget {
  @override
  _CitiesPageState createState() => _CitiesPageState();
}

class _CitiesPageState extends State<CitiesPage> {
  Completer<void> _refreshCompleter;
  final _scrollController = ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    final windBloc = BlocProvider.of<WindBloc>(context);
    windBloc.dispatch(Fetch());
    return BlocListener(
        listener: (BuildContext context, state) {
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
        },
        bloc: windBloc,
        child: BlocBuilder(
            bloc: windBloc,
            builder: (BuildContext context, WindState state) {
              if (state is WindUninitialized) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return RefreshIndicator(
                    key: _refreshIndicatorKey,
                    onRefresh: () {
                      windBloc.dispatch(Fetch());
                      return _refreshCompleter.future;
                    },
                    child: Builder(builder: (BuildContext context) {
                      if (state is WindError) {
                        return ScrollableText(
                            'Failed to fetch wind for cities');
                      } else {
                        if (state is WindLoaded) {
                          if (state.windList.isEmpty) {
                            return ScrollableText('Add a city');
                          }
                          return ListView.builder(
                            itemBuilder: (BuildContext context, int index) {
                              return index >= state.windList.length
                                  ? BottomLoader()
                                  : Slidable(
                                      key: ValueKey(index),
                                      child: WindTile(
                                        city: state.windList[index].city,
                                        wind: state.windList[index].wind,
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  BlocProvider(
                                                builder: (context) => ForecastBloc(
                                                    forecastRepository:
                                                        ForecastRepository(
                                                            weatherApiClient:
                                                                WeatherApiClient(
                                                                    httpClient:
                                                                        http.Client()))),
                                                child: ForecastPage(
                                                    state.windList[index].city),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                      actionPane: SlidableDrawerActionPane(),
                                      secondaryActions: <Widget>[
                                        IconSlideAction(
                                          caption: 'Delete',
                                          color: Colors.red,
                                          icon: Icons.delete,
                                          closeOnTap: true,
                                          onTap: () {
                                            print(
                                                "${DateTime.now().toIso8601String()} Delete onTap");
                                            windBloc.dispatch(DeleteCity(
                                                state.windList[index].city));
                                          },
                                        ),
                                      ],
                                      dismissal: SlidableDismissal(
                                        child: SlidableDrawerDismissal(),
                                      ),
                                    );
                            },
                            itemCount: state.hasReachedMax
                                ? state.windList.length
                                : state.windList.length + 1,
                            controller: _scrollController,
                            physics: AlwaysScrollableScrollPhysics(),
                          );
                        } else {
                          throw Exception("Invalid State");
                        }
                      }
                    }));
              }
            }));
  }
}

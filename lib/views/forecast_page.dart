import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wind/bloc/forecast/forecast_bloc.dart';
import 'package:flutter_wind/bloc/forecast/forecast_event.dart';
import 'package:flutter_wind/bloc/forecast/forecast_state.dart';
import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/widgets/forecast_tile.dart';
import 'package:flutter_wind/widgets/scrollable_text.dart';

class ForecastPage extends StatefulWidget {
  final City city;

  ForecastPage(this.city);

  @override
  _ForecastPageState createState() => _ForecastPageState(city);
}

class _ForecastPageState extends State<ForecastPage> {
  final City city;
  Completer<void> _refreshCompleter;
  final _scrollController = ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  _ForecastPageState(this.city);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    final forecastBloc = BlocProvider.of<ForecastBloc>(context);
    forecastBloc.dispatch(Fetch(city));
    return Scaffold(
      appBar: AppBar(
        title: Text('Forecast for ${city.name}'),
      ),
      body: BlocListener(
          listener: (BuildContext context, state) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          },
          bloc: forecastBloc,
          child: BlocBuilder(
              bloc: forecastBloc,
              builder: (BuildContext context, ForecastState state) {
                if (state is ForecastUninitialized) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return RefreshIndicator(
                      key: _refreshIndicatorKey,
                      onRefresh: () {
                        forecastBloc.dispatch(Fetch(city));
                        return _refreshCompleter.future;
                      },
                      child: Builder(builder: (BuildContext context) {
                        if (state is ForecastError) {
                          return ScrollableText(
                              'Failed to fetch wind for cities');
                        } else {
                          if (state is ForecastLoaded) {
                            if (state.windList.isEmpty) {
                              return ScrollableText('No Forecast Available');
                            }
                            return ListView.builder(
                              itemBuilder: (BuildContext context, int index) {
                                return ForecastTile(
                                  wind: state.windList[index],
                                );
                              },
                              itemCount: state.windList.length,
                              controller: _scrollController,
                              physics: AlwaysScrollableScrollPhysics(),
                            );
                          } else {
                            throw Exception("Invalid State");
                          }
                        }
                      }));
                }
              })),
    );
  }
}

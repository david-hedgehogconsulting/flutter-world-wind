import 'dart:async';

import 'package:flutter_wind/data/wind_database.dart';
import 'package:meta/meta.dart';
import 'package:moor_flutter/moor_flutter.dart';

class SearchRepository {
  final LookupCityDao lookupCitiesDao;

  SearchRepository({@required this.lookupCitiesDao});

  Future<List<LookupCity>> searchForCities(String searchString) {
    return lookupCitiesDao.searchForCities(searchString);
  }
}

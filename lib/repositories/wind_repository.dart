import 'dart:async';

import 'package:flutter_wind/data/wind_database.dart';
import 'package:flutter_wind/model/city_wind.dart';
import 'package:flutter_wind/model/wind.dart';
import 'package:flutter_wind/services/weather_api_client.dart';
import 'package:meta/meta.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:rxdart/rxdart.dart';

class WindRepository {
  final CitiesDao citiesDao;
  final WeatherApiClient weatherApiClient;
  final BehaviorSubject<List<Wind>> _weatherSubject =
      BehaviorSubject.seeded(List());

  WindRepository({@required this.citiesDao, @required this.weatherApiClient})
      : assert(weatherApiClient != null);

  Observable<List<Wind>> observeWind() {
    return Observable(citiesDao.observeCities())
        .flatMap((cities) => Stream.fromFuture(fetchWindForCities(cities)));
  }

  Future<List<Wind>> fetchWind() async {
    final List<City> cities = await citiesDao.fetchCities();
    return cities.isEmpty ? [] : await fetchWindForCities(cities);
  }

  Future<List<Wind>> fetchWindForCities(List<City> cities) async {
    if (cities.isNotEmpty) {
      final List<int> cityIds = cities.map((city) => city.id).toList();
      return await weatherApiClient.fetchWind(cityIds);
    } else {
      return List();
    }
  }

  Observable<List<CityWind>> observeCityWind() {
    return Observable.combineLatest2(
        Observable(citiesDao.observeCities())
            .doOnData((cities) => fetchWindForCities(cities).then(
                  (windList) => _weatherSubject.add(windList),
                )),
        _weatherSubject, (List<City> cities, List<Wind> windList) {
      return cities
          .map((city) => CityWind(
              city,
              windList.firstWhere((wind) => city.id == wind.id,
                  orElse: () => null)))
          .toList();
    });
  }

  Future<List<CityWind>> fetchCityWind() async {
    final List<City> cities = await citiesDao.fetchCities();
    return cities.isEmpty ? [] : await fetchCityWindForCities(cities);
  }

  Future<List<CityWind>> fetchCityWindForCities(List<City> cities) async {
    if (cities.isNotEmpty) {
      final List<int> cityIds = cities.map((city) => city.id).toList();
      final List<Wind> windList = await weatherApiClient.fetchWind(cityIds);
      return windList
          .map((wind) =>
              CityWind(cities.firstWhere((city) => city.id == wind.id), wind))
          .toList();
    } else {
      return [];
    }
  }

  Future<int> deleteCity(City city) async {
    return await citiesDao.deleteCity(city);
  }

  Future<int> insertLookupCity(LookupCity city) async {
    return await citiesDao.insertCity(
      CitiesCompanion(
        id: Value(city.id),
        name: Value(city.name),
        country: Value(city.country),
      ),
    );
  }
}

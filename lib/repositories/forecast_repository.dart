import 'package:flutter_wind/model/wind.dart';
import 'package:flutter_wind/services/weather_api_client.dart';
import 'package:meta/meta.dart';

class ForecastRepository {
  final WeatherApiClient weatherApiClient;

  ForecastRepository({@required this.weatherApiClient});

  Future<List<Wind>> fetchForecastForCity(int cityId) async {
    return await weatherApiClient.fetchForecastForCity(cityId);
  }
}
